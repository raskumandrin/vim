execute pathogen#infect()

au BufNewFile,BufRead *.html.ep set filetype=html  " Mojolicious
au BufNewFile,BufRead *.thtml set filetype=perl    " 220

"autocmd QuickFixCmdPost *grep* cwindow
autocmd QuickFixCmdPost l* lwindow
autocmd QuickFixCmdPost [^l]* cwindow

noremap <Up> <nop>
noremap <Down> <nop>
noremap <Left> <nop>
noremap <Right> <nop>

set fileencodings=utf-8,cp1251

set tabstop=4
set shiftwidth=4
set smarttab
set expandtab

set laststatus=2

"Give a shortcut key to NERD Tree
map <F2> :NERDTreeToggle<CR>

"Give a shortcut key to Soft wrap 
:function ToggleWrap()
: if (&wrap == 1)
:   set nowrap
: else
:   set wrap
: endif
:endfunction

map <F9> :call ToggleWrap()<CR>
map! <F9> ^[:call ToggleWrap()<CR>

set showbreak=\\\\\\>
set linebreak



set wildmenu
set wcm=<Tab>
menu Paste.before :set paste<CR>
menu Paste.after  :set nopaste<CR>
map <F12> :emenu Paste.<Tab>



" https://gist.github.com/jbolila/7598018

nmap <F8> :TagbarToggle<CR>

let g:tagbar_autoclose = 1
" Tagbar settings to recognise Perl Moo..Moose
let g:tagbar_type_perl = {
    \ 'ctagstype'   : 'Perl',
    \ 'kinds' : [
        \ 'p:packages:1:0',
        \ 'u:uses:1:0',
        \ 'r:requires:1:0',
        \ 'e:extends',
        \ 'w:roles',
        \ 'o:ours:1:0',
        \ 'c:constants:1:0',
        \ 'f:formats:1:0',
        \ 'a:attributes',
        \ 's:subroutines',
        \ 'x:around:1:0',
        \ 'l:aliases',
        \ 'd:pod:1:0',
    \ ],
\ }



syntax on

set background=light
let g:solarized_termcolors=256
let g:solarized_termtrans=1
let g:solarized_degrade=1
let g:solarized_visibility = "high"
let g:solarized_contrast="high"
colorscheme solarized



" start of colors for line numbers
" http://stackoverflow.com/questions/8247243/highlighting-the-current-line-number-in-vim

set number

set cursorline

hi clear CursorLine
augroup CLClear
    autocmd! ColorScheme * hi clear CursorLine
augroup END

augroup CLNRSet
    autocmd! ColorScheme * hi CursorLineNR cterm=bold 
augroup END

hi LineNr ctermbg=254

" end of colors for line numbers





" vim bundle/vim-perl/syntax/perl.vim 

hi MatchParen ctermbg=231

" Syntax: {{{1
hi Normal  ctermfg=0 
hi Boolean  ctermfg=69
hi Character  ctermfg=160
hi Comment  ctermfg=244
hi Conditional  ctermfg=27 
hi Constant  ctermfg=160
hi Define  ctermfg=27
hi ErrorMsg  ctermfg=1 
hi WarningMsg  ctermfg=1 
hi Float  ctermfg=19
hi Function  ctermfg=19 
hi Identifier  ctermfg=0
hi Keyword        ctermfg=21 
hi Label  ctermfg=28
hi Number  ctermfg=0
hi Operator  ctermfg=27
hi PreProc  ctermfg=27
hi Special ctermfg=0
hi Statement  ctermfg=27 
hi StorageClass  ctermfg=27
hi String  ctermfg=22
hi Title  ctermfg=0 
hi Todo  ctermfg=27
hi Underlined  gui=underline

hi perlIdentifier2 ctermfg=6
hi! link Identifier perlIdentifier2 
hi! link  perlVarPlain perlIdentifier2 
hi! link  perlPackageRef perlIdentifier2 

hi  perlStatement2 ctermfg=19
hi! link perlStatement perlStatement2 


" Diff styles {{{1
hi diffAdded guifg=#007B22 guibg=#FFFFFF ctermfg=28 ctermbg=15
hi diffRemoved guifg=#D51015 guibg=#FFFFFF ctermfg=160 ctermbg=15
hi diffFile guifg=#6E79F1 guibg=#FFFFFF ctermfg=69 ctermbg=15
hi diffNewFile guifg=#6E79F1 guibg=#FFFFFF ctermfg=69 ctermbg=15
hi diffLine guifg=#000000 guibg=#FFFFFF ctermfg=0 ctermbg=15

hi diffAdd guifg=#007B22 guibg=#FFFFFF ctermfg=28 ctermbg=15
hi diffChange guifg=#007B22 guibg=#FFFFFF ctermfg=28 ctermbg=15
hi diffText guifg=#007B22 guibg=#FFFFFF ctermfg=28 ctermbg=15
hi link diffDelete diffRemoved
" Git styles {{{1
hi gitcommitFirstLine guifg=#000000 guibg=#FFFFFF ctermfg=0 ctermbg=15
hi gitcommitSelectedType guifg=#007B22 guibg=#FFFFFF ctermfg=28 ctermbg=15
hi link gitcommitSelectedFile gitcommitSelectedType
hi gitcommitDiscardedType guifg=#D51015 guibg=#FFFFFF ctermfg=160 ctermbg=15
hi link gitcommitDiscardedFile gitcommitDiscardedType
hi gitcommitUntrackedFile guifg=#6E79F1 guibg=#FFFFFF ctermfg=69 ctermbg=15


" XML: {{{1
hi link xmlEndTag xmlTag
" HTML: {{{1
hi htmlTag  guifg=#1E39F6 ctermfg=27
hi link htmlEndTag htmlTag
hi link htmlTagName htmlTag
hi link htmlArg htmlTag
hi htmlSpecialChar  guifg=#D51015 ctermfg=160 gui=bold
hi htmlH1 gui=bold
hi link htmlH2 htmlH1
hi link htmlH3 htmlH1
hi link htmlH4 htmlH1
hi link htmlH5 htmlH1
hi link htmlH6 htmlH1

" JavaScript: {{{1
hi javaScriptFunction  guifg=#1E39F6 ctermfg=27 gui=bold
hi javaScriptFuncName  guifg=#318495 ctermfg=66 gui=italic
hi javaScriptLabel  guifg=#0000A2 ctermfg=19 gui=bold
hi javaScriptRailsFunction  guifg=#3C4C72 ctermfg=240 gui=bold
hi javaScriptType  guifg=#318495 ctermfg=66 gui=none
hi javaScriptArgument  guifg=#318495 ctermfg=66 gui=italic
hi javaScriptRegexpString  guifg=#E18AC7 ctermfg=176 gui=NONE
hi javaScriptSpecial  guifg=#00BC41 ctermfg=35 gui=NONE
" NOTE: Syntax Highlighting for javascript doesn't match the
" TextMate version very accurately, because the javascript
" syntax file does not create matches for some items. In
" particular:
" * javaScriptArgument - e.g. function(argument)
" * javaScriptFuncName for object literal style functions - e.g.: 
"     myFunction: function() { ... }


" CSS: {{{1
hi cssTagName  gui=bold
hi cssIdentifier  gui=italic
hi link cssClassName cssIdentifier
hi cssDefinition  guifg=#6E79F1 ctermfg=69
hi link cssRenderProp cssDefinition
hi link cssTextProp cssDefinition
hi link cssFontProp cssDefinition
hi link cssColorProp cssDefinition
hi link cssBoxProp cssDefinition
hi link cssGeneratedContentProp cssDefinition
hi link cssUIProp cssDefinition
hi cssCommonAttr  guifg=#00BC41 ctermfg=35
hi link cssAttr cssCommonAttr
hi link cssRenderAttr cssCommonAttr
hi link cssTextAttr cssCommonAttr
hi link cssFontAttr cssCommonAttr
hi link cssGeneratedContentAttr cssCommonAttr
hi cssURL  guifg=#007B22 ctermfg=28
hi cssFunctionName  guifg=#3C4C72 ctermfg=240 gui=bold
hi cssColor  guifg=#D51015 ctermfg=160 gui=bold
hi cssValueLength  guifg=#0000A2 ctermfg=19
hi cssImportant  guifg=#1E39F6 ctermfg=27 gui=bold

" Vimscript: {{{1
hi vimGroup  guifg=#007B22 ctermfg=28 gui=bold
hi link vimHiGroup vimGroup
hi vimCommentTitle  guifg=#3C4C72 ctermfg=240 gui=bold
hi helpSpecial guifg=#6E79F1 ctermfg=69

" Markdown: {{{1
hi markdownBold gui=bold
hi markdownItalic gui=italic
hi markdownCode  guifg=#007B22 ctermfg=28
hi link markdownCodeBlock markdownCode

" Outliner: {{{1
hi BT1 guifg=#808080 ctermfg=244 gui=italic
hi OL1 guifg=#000000 ctermfg=0 gui=bold
hi OL2 guifg=#0000A2 ctermfg=19 gui=bold
hi OL3 guifg=#007B22 ctermfg=28 gui=bold
hi OL4 guifg=#6E79F1 ctermfg=69 gui=NONE
" Modelines: {{{1
" vim: nowrap fdm=marker
" }}}




hi! VertSplit    ctermfg=lightgray ctermbg=gray cterm=reverse
hi! Folded       ctermfg=blue ctermbg=lightcyan
hi! IncSearch    ctermfg=lightgray ctermbg=yellow
hi! Visual       ctermfg=blue ctermbg=white cterm=reverse
"hi! Comment      ctermfg=green
hi! Constant     ctermfg=blue
"hi! String       ctermfg=red
hi! Float        ctermfg=red
hi! Boolean      ctermfg=darkblue cterm=bold
hi! Statement    ctermfg=darkblue cterm=bold
hi! PreProc      ctermfg=cyan
hi! Type         ctermfg=darkblue cterm=bold
hi! Special      ctermfg=brown
hi! Underlined   ctermfg=blue
hi! Todo         ctermfg=red ctermbg=yellow

hi! perlSharpBang ctermfg=240 
let perl_no_extended_vars=1

" https://gist.github.com/Ovid/386157c880221ac72fbc

" Drop this into .vim/plugin.vawa.vim
" if you already have vawa.vim, rename appropriately
" automatically highlights variables under cursor, allowing you to easily see the data flow.

" Vawa Plugin for VIM > 7.3 version 1.00
" Maintainer: Sandeep.c.r<sandeepcr2@gmail.com>
" Hacked for Perl by Curtis "Ovid" Poe <ovid@allaroundtheworld.fr>

function! s:vawa()
    call clearmatches()
    let s:temp          = getpos('.')
    let s:current_col   = s:temp[2]
    let s:current_line  = s:temp[1]
    let s:temp          = searchpos('[>$@%*&]','bcn')
    let s:sigil_line    = s:temp[0]
    let s:sigil_col     = s:temp[1]
    if(s:sigil_line != s:current_line)
        return
    endif
    let s:temp = getbufline(bufnr('%'),line('.'))
    let s:varend = match(s:temp[0], '[^a-zA-Z0-9_\x7f-\xff]\|\n', s:sigil_col)+1
    let s:space_col = s:varend
    if((s:space_col > s:sigil_col) && ( s:space_col< s:current_col))
        return
    endif
    if(s:varend == 0)
        let s:varend = strlen(s:temp[0])+1
    endif
    let s:current_char = s:temp[0][s:current_col-1]
    if(s:current_char == '>' || s:current_char == '-')
        return
    endif
    if(exists("g:vawahl"))
        exe "highlight VarHl  " . g:vawahl
    else
        highlight VarHl  ctermbg=224
    endif
    let s:str = strpart(s:temp[0], s:sigil_col-1,(s:varend - s:sigil_col))
    let s:prefix = ''
    if(exists("g:vawanohlcurrent") && (g:vawanohlcurrent == 1))
        let s:lineab = s:current_line - 1
        let s:linebe = s:current_line + 1
        let s:colbf  = s:sigil_col + 1
        let s:prefix = '\(\%>'.s:lineab.'l.\%<'.s:linebe.'l.\%>'.s:colbf.'c\)\@!'
    endif
    if(s:str == '$')
        return
    endif
    if(strpart(s:str,0,1) == '>')
        let s:str = strpart(s:str ,1)
        call matchadd('VarHl', s:prefix.'>\@<='. s:str.'\n\{-\}\(\([^a-zA-Z0-9_\x7f-\xff]\)\|$\)\@=')
    else
        call matchadd('VarHl', s:prefix.s:str.'\n\{-\}\(\([^a-zA-Z0-9_\x7f-\xff]\)\|$\)\@=')
    endif
endfunction
if(!exists("g:vawanoauto") || (g:vawanoauto == 1))
    augroup HighlightVar
            autocmd!
            au CursorMoved  *.pl call <sid>vawa()
            au CursorMovedi *.pl call <sid>vawa()
            au CursorMoved  *.pm call <sid>vawa()
            au CursorMovedi *.pm call <sid>vawa()
            au CursorMoved  *.t  call <sid>vawa()
            au CursorMovedi *.t  call <sid>vawa()
    augroup END
endif

command! Vawa :call <sid>vawa()

