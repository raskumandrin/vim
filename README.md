Installation
============

Vim for Mac: http://macvim-dev.github.io/macvim/

Clone repo:

    git clone https://raskumandrin@bitbucket.org/raskumandrin/vim.git ~/.vim

or

    git clone git@bitbucket.org:raskumandrin/vim.git ~/.vim

Create symlinks:

    ln -s ~/.vim/vimrc ~/.vimrc
    ln -s ~/.vim/tmux.conf ~/.tmux.conf

Switch to the `~/.vim` directory, and fetch submodules:

    cd ~/.vim
    git submodule init
    git submodule update

Later on to update all bundled plugins:

    git submodule foreach git pull origin master

Vim tips
========

[Vim anti-patterns](https://sanctum.geek.nz/arabesque/vim-anti-patterns/)


*****

*****

*****


Colouring
=========

![Colors 256](https://bitbucket.org/raskumandrin/vim/raw/master/colors256.png)

    #!/usr/bin/env perl

    use strict;
    use warnings;

    my $fg = "\x1b[38;5;";
    my $bg = "\x1b[48;5;";
    my $rs = "\x1b[0m";

    my $color = 0;

    for (my $row = 0; $row < 32; ++$row) {
      for (my $col = 0; $col < 8; ++$col) {
        print get_color($color);
        $color++;
      }
      print "\n";
    }

    sub get_color {
      my ($color) = @_;
      my $number = sprintf '%3d', $color;
      return qq/${bg}${color}m ${number} ${rs} ${fg}${color}m${number}${rs} /;
    }


Improvement
===========

Install plugins as submodules:

    cd ~/.vim
    mkdir ~/.vim/bundle
    git submodule add git://github.com/altercation/vim-colors-solarized.git bundle/solarized
    git add .
    git commit -m "Install Solarized Colorscheme bundle as a submodule."


http://vimcasts.org/episodes/synchronizing-plugins-with-git-submodules-and-pathogen/

http://blog.smalleycreative.com/tutorials/using-git-and-github-to-manage-your-dotfiles/

http://vim.wikia.com/wiki/Xterm256_color_names_for_console_Vim
http://terminal-color-builder.mudasobwa.ru
http://habrahabr.ru/post/161999/

Installing Pathogen (already included in this repository)

https://github.com/tpope/vim-pathogen

    mkdir -p ~/.vim/autoload ~/.vim/bundle && \
    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

Delete git submodule:

To remove a submodule you need to:

    Delete the relevant section from the .gitmodules file.
    Stage the .gitmodules changes git add .gitmodules
    Delete the relevant section from .git/config.
    Run git rm --cached path_to_submodule (no trailing slash).
    Run rm -rf .git/modules/path_to_submodule
    Commit git commit -m "Removed submodule <name>"
    Delete the now untracked submodule files
    rm -rf path_to_submodule
    Run rm -Rf .git/modules/path_to_submodule
